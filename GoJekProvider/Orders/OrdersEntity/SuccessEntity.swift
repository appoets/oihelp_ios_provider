//
//  SuccessEntity.swift
//  GoJekProvider
//
//  Created by CSS on 20/04/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct SuccessEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [String]?
    var error : [String]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
    
}

struct CashFreeEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : CashFreeResponseData?
    var error : [String]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
    
}

struct CashFreeResponseData : Mappable {
    var id : Int?
    var company_id : Int?
    var admin_service : String?
    var is_wallet : Int?
    var user_type : String?
    var payment_mode : String?
    var user_id : Int?
    var amount : Int?
    var cftoken : String?
    var transaction_code : String?
    var status : String?
    var order_id : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        company_id <- map["company_id"]
        admin_service <- map["admin_service"]
        is_wallet <- map["is_wallet"]
        user_type <- map["user_type"]
        payment_mode <- map["payment_mode"]
        user_id <- map["user_id"]
        amount <- map["amount"]
        cftoken <- map["cftoken"]
        transaction_code <- map["transaction_code"]
        status <- map["status"]
        order_id <- map["order_id"]
    }
    
}
